

export enum Commands {
    Create = "create",
    Read = "read",
    Update = "update",
    Delete = "delete",
    Help = "help",
}

export enum CommandParams {
    Create_P = "-t --title",
    Read_P = "-a --all -o --open -c --completed",
    Update_P = "-i --id",
    Delete_P = "-i --id",
}

// const generateUniqueId = () => Math.random().toString(36).substring(2);
export const generateUniqueId = () => Math.random().toString(36).slice(-4);