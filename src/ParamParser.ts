import {Commands, CommandParams} from "./Utils.js";

export function validateParams(command: string, ...args: any[]) : boolean {
    let isValid: boolean = false;

    switch(command) {
        case Commands.Create:
            break;
        case Commands.Read:
            break;
        case Commands.Update:
            break;
        case Commands.Delete:
            break;
        case Commands.Help:
            break;
        default:
            break;
    }

    return isValid;
}