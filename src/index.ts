// the first two arguments in process.argv are "node" and the path to the current folder!

import { F_OK } from "constants";
import fs from "fs/promises";
import fs_sync from "fs";
import log from "@ajar/marker";
import {Commands, CommandParams, generateUniqueId} from "./Utils.js";


/*
   Expected Args:
 * -- create --taskText
 * -- read
 * -- update --taskId --value (complete/not)
 * -- delete --taskId
 * -- help  ---> displays the possible commands & parameters to use my script
*/

init();

interface TaskObj {
    id: string;
    text: string;
    isCompleted: boolean;
}

// enum Commands {
//     Create = "create",
//     Read = "read",
//     Update = "update",
//     Delete = "delete",
//     Help = "help",
// }

async function init() {
    let myArgs = process.argv.slice(2); // the first two args in process.argv are "node" and the path to the current folder
    console.log("myArgs = ", myArgs);

    let appState: TaskObj[];
    appState = await getStateFromDB("todos.json"); // check if "todos.json" exists, and create it if it doesn't

    // log.magenta("appState = ", JSON.stringify(appState));

    let command: string = myArgs[0];
    let cmdArgs;
    console.log("command = ", command);
    switch (command) {
        case Commands.Create:
            console.log("creating new task");
            cmdArgs = myArgs[1].slice(2);  // cut the "--"
            createNewTask(cmdArgs, "todos.json");
            break;
        case Commands.Read:
            console.log("reading all tasks");
            readTasks("todos.json", appState);
            break;
        case Commands.Update:
            console.log("updating the completed-flag");
            cmdArgs = myArgs[1].slice(2);  // cut the "--"
            updateTaskCompletionStatus(cmdArgs, "todos.json");
            break;
        case Commands.Delete:
            cmdArgs = myArgs[1].slice(2);  // cut the "--"
            deleteTask(cmdArgs, "todos.json");
            break;
        case Commands.Help:
            log.green(`Usage of the To Do App:`);
            break;
        default:
            log.red("Unrecognized Command! please try again.");
            break;
    }
}

async function getStateFromDB(db: string): Promise<TaskObj[]> {
    // let tasksData;

    try {
        let tasksData = await fs.readFile("todos.json", "utf-8");
        return JSON.parse(tasksData);
    } catch (error) {
        // console.log(error);
        await fs.writeFile("todos.json", "", "utf-8");
        return [];
    }
    // log.green("inside getState! state = ", state);
}

async function updateStateInDB(db: string, state: TaskObj[]) {
    await fs.writeFile(db, JSON.stringify(state, null, 2), "utf-8");
}

async function createNewTask(taskText: string, db: string) {
    let task: TaskObj = { id: generateUniqueId(), text: taskText, isCompleted: false };
    let state: TaskObj[] = await getStateFromDB(db);
    // log.yellow("state before push: ", state);
    state.push(task);
    // console.log("inside create after push! state = ", JSON.stringify(state));
    // log.cyan("inside create", "state = ", JSON.stringify(state), "task: ", JSON.stringify(task));
    // await fs.writeFile(db, JSON.stringify(state, null, 2), "utf-8");
    updateStateInDB(db, state);
}

async function readTasks(db: string, state: TaskObj[]): Promise<void> {
    log.magenta("☆ To Do List ☆");
    log.cyan("--------------");

    state = await getStateFromDB(db);
    state.forEach((task) =>  console.log(`id: ${task.id}   Task: ${task.text}      Done? ${task.isCompleted ? "V" : "X"}`));
}

async function updateTaskCompletionStatus(taskId: string, db: string) {
    let state: TaskObj[] = await getStateFromDB(db);

    state.map((task) => {
        if (task.id === taskId) { task.isCompleted = !task.isCompleted; }
    });

    // await fs.writeFile(db, JSON.stringify(state, null, 2), "utf-8");
    updateStateInDB(db, state);
}

async function deleteTask(taskId: string, db: string) {
    let state: TaskObj[] = await getStateFromDB(db);
    state = state.filter((task) => task.id !== taskId);
    // console.log("inside delete! taskId = ", taskId, "typeof taskId=", typeof taskId, ", state = ", state);
    // await fs.writeFile(db, JSON.stringify(state, null, 2), "utf-8");
    updateStateInDB(db, state);
}